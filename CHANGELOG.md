# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [2.2] - 2021-04-06
### Added
- Landscape templates for the 2mm and 3mm metric templates

## [2.1] - 2021-04-04
### Changed
- Adjusted the positioning of lines in the templates to align with the
  pixels on the reMarkable's screen.

## [2.0] - 2021-04-03
### Changed
- Templates are now generated by a program from a short list of
  parameters, in place of the old handmade SVGs.

## [1.0] - 2021-04-01
### Added
- Initial public release

[Unreleased]: https://gitlab.com/asciiphil/remarkable-engineering/-/compare/2.2...master
[2.2]: https://gitlab.com/asciiphil/remarkable-engineering/-/compare/2.1...2.2
[2.1]: https://gitlab.com/asciiphil/remarkable-engineering/-/compare/2.0...2.1
[2.0]: https://gitlab.com/asciiphil/remarkable-engineering/-/compare/1.0...2.0
[1.0]: https://gitlab.com/asciiphil/remarkable-engineering/-/tree/1.0
